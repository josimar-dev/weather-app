import React, { Component } from 'react';

// CONTAINERS
import Searchbar from '../containers/search_bar';

export default class App extends Component {
  render() {
    return (
      <div><Searchbar /></div>
    );
  }
}
